#!/usr/bin/env bash

if [ ! -d pkg/.tmp ]; then
mkdir pkg/.tmp
fi
if [ ! -d pkg/.tmp/balance ]; then
mkdir pkg/.tmp/balance
 if [ ! -d pkg/.tmp/balance/DW_DLC47 ]; then
	mkdir pkg/.tmp/balance/DW_DLC47
 fi
fi
if [ ! -d pkg/.tmp/performance ]; then
mkdir pkg/.tmp/performance
 if [ ! -d pkg/.tmp/performance/DW_DLC47 ]; then
	mkdir pkg/.tmp/performance/DW_DLC47
 fi
fi

cp out/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak

cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataBr.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataCn.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataCs.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataDe.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataEl.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataEn.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataEs.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataFr.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataIt.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataJp.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataKo.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataNl.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataPl.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataRu.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataTh.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataTr.pak
cp pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/balance/DW_DLC47/DataTw.pak
rm -rf pkg/.tmp/balance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak

cp out/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak

cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataBr.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataCn.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataCs.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataDe.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataEl.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataEn.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataEs.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataFr.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataIt.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataJp.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataKo.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataNl.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataPl.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataRu.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataTh.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataTr.pak
cp pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak pkg/.tmp/performance/DW_DLC47/DataTw.pak
rm -rf pkg/.tmp/performance/DW_DLC47/DataBr-Cn-Cs-De-El-En-Es-Fr-It-Jp-Ko-Nl-Pl-Ru-Th-Tr-Tw.pak
