Copyright :copyright: 2022 [`gentoolinux-girl`](https://gitlab.com/gentoolinux-girl), & [`dlim`](https://gitlab.com/dlimi/dlim) and [`dlimcsm`](https://gitlab.com/dlimi/dlimcsm) contributors.

### Warranty and liability

- This project comes with no warranty, explicitly or implicitly.

- No contributors or developers are responsible for the users usage. Nor are they liable for any damages caused by misuse.

### Usage and distribution

- Anyone may use [`dlimcsm`](https://gitlab.com/dlimi/dlimcsm) free of charge.

- Anyone may freely distribute and redistribute copies.

- Modified redistributions must be plainly marked as such.

- Anyone may not claim ownership of the entirety of [`dlimcsm`](https://gitlab.com/dlimi/dlimcsm), with the exception of the founder [`gentoolinux-girl`](https://gitlab.com/gentoolinux-girl). In the event that such classifications are necessary.

- Proprietary projects must open-source the portion of the program interfacing with this project.

### All rights reserved

The original scripts/sources belong to [**Techland**](https://techland.net), created by their talented engineers, and artists; Modified by [gentoolinux-girl](https://gitlab.com/gentoolinux-girl), with help from many other contributors. All rights reserved to their original owners.
