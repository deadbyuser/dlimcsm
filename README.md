<details><!--English-->
<summary><h2>:flag_gb: :flag_au: English :flag_nz: :flag_us:</h2></summary>

## 🍃 Dying Light Improved Compatibility Support Module (*`dlimcsm`*) 🛠️

[`dlim`](https://gitlab.com/dlimi/dlim)-based efficiency and performance enhancement to [**Dying Light**](https://dl1.dyinglightgame.com).
#

<details><!--English::Installation-->
<summary><h2>📥 Installation 🗜️</h2></summary>

<details><!--English::Installation::Stable-->
<summary><h2>💼 Stable 📥</h2></summary>

#
</details><!--English::Installation::Stable-->

<details><!--English::Installation::Development-->
<summary><h2>🚧 Development 📥</h2></summary>


<details><!--English::Installation::Development::Linux-->
<summary><h2>🚧 Linux 🐧</h2></summary>

<details><!--English::Installation::Development::Linux::Steam with Proton-->
<summary><h3>⚛️ Steam with Proton 🐧</h3></summary>

#### `1`: In Steam, right click `Dying Light` and go to `Properties`.

![](img/en/navigate-to-properties.png)

#### `2`: In `Properties`, navigate to `COMPATIBILITY`.

![](img/en/navigate-to-compatibility.png)

#### `3`: Enable `Proton`

![](img/en/enable-proton.png)

#### `4`: Navigate to `GENERAL`.

![](img/en/navigate-to-general.png)

#### `5`: Set your launch options to enable key features:

```sh
PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0 %command% -nologos -fsreadmembuffer
```

##### Confused? In your Library, Right click `Dying Light`, then navigate to `Properties`.

![](img/en/navigate-to-properties.png)

##### Then you should see the launch options section.

![](img/en/notice-launch-options.png)

> #### ⚠️ Notice! ❕
>
> `DXVK_ASYNC` is strongly suggested; It is the de-facto way to use `dlimcsm` & `dlim`.
>
> It heavily reduces initial stuttering for `pipeline-state-cache`.
>
> It is enabled by default in `dxvk.conf`; However it requires [`GE-Proton`](https://github.com/gloriouseggroll/proton-ge-custom), or [`DLDXVK`](https://gitlab.com/dlimi/dldxvk).

<details><!--English::Installation::Development::Linux::Steam with Proton::Nvidia Tweaks-->
<summary><h4>🐸 Nvidia Tweaks ⚡</h4></summary>

> #### ⚠️ Notice! ❕
>
> Append `PROTON_HIDE_NVIDIA_GPU=0` and `PROTON_ENABLE_NVAPI=1` to your launch options:
>
> ```sh
> PROTON_HIDE_NVIDIA_GPU=0 PROTON_ENABLE_NVAPI=1 PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0 %command% -nologos -fsreadmembuffer
> ```

#
</details><!--English::Installation::Development::Linux::Steam with Proton::Nvidia Tweaks-->

<details><!--English::Installation::Development::Linux::Steam with Proton::FSRHack-->
<summary><h4>📹 FSRHack 🔍</h4></summary>

> #### ⚠️ Notice! ❕
>
> To get FSR, first append `WINE_FULLSCREEN_FSR=1` and `WINE_FULLSCREEN_FSR=2` (*`0`, strongest, to `5`, weakest*) to your launch options:
>
> ```sh
> WINE_FULLSCREEN_FSR=1 WINE_FULLSCREEN_FSR_STRENGTH=2 PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0 %command% -nologos -fsreadmembuffer
> ```
>
> Or with an Nvidia GPU:
>
> ```sh
> WINE_FULLSCREEN_FSR=1 WINE_FULLSCREEN_FSR_STRENGTH=2 PROTON_HIDE_NVIDIA_GPU=0 PROTON_ENABLE_NVAPI=1 PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0 %command% -nologos -fsreadmembuffer
> ```
#
</details><!--English::Installation::Development::Linux::Steam with Proton::FSRHack-->

#### `6`: Download a build:

File                                                                                                                    | Description
------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------
[`dlimcsm-balance.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-balance.pkg.7z?inline=false)         | Cleanly balanced optimizations with minimal compromise.
[`dlimcsm-performance.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-performance.pkg.7z?inline=false) | Full performance optimizations with maximum compromise.

#### `7`: In your Downloads folder, double click the `.7z` file to open it.

![](img/en/open-the-archive.png)

#### `8`: Drop the `dxvk.conf` and `DW_DLC47` file into the `/Dying Light/` directory.

![](img/en/install-the-package.png)

#

### 🌬️ Final notes 🗒️

+ In case you are unfamiliar with `DXVK`; There will be stutters for every un-rendered scene, as the shaders are compiled.
	- This stuttering should go away quickly. :)

+ Render distance will be more accurate. Slightly increase render distance to have old ranges.

+ This work is very tiring. So I hope you enjoy it. :3
	- ![](img/sleepy-cat.gif)

#
</details><!--English::Installation::Development::Linux::Steam with Proton-->

#
</details><!--English::Installation::Development::Linux-->

<details><!--English::Installation::Development::Windows-->
<summary><h2>🚧 Windows 🪟</h2></summary>

<details><!--English::Installation::Development::Windows::Steam-->
<summary><h3>⚛️ Steam 🪟</h3></summary>

#### `1`: Install `7zip` if not installed.

If you do not have `7zip`, [click here](https://7-zip.org/a/7z2107-x64.exe) to download the installer. Then run it from your `/Downloads` folder.

#### `2`: Download a build:

File                                                                                                                                  | Description
--------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------
[`dlimcsm-balance-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-balance-dldxvk.pkg.7z?inline=false)         | Cleanly balanced optimizations with minimal compromise including `DLDXVK`.
[`dlimcsm-performance-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-performance-dldxvk.pkg.7z?inline=false) | Full performance optimizations with maximum compromise including `DLDXVK`.

#### `3`: In your Downloads folder, double click the `.7z` file to open it.

![](img/en/open-the-archive-including-dldxvk.png)


#### `4`: Drop the `dxgi.dll`, `d3d11.dll`, `dxvk.conf` files, and `DW_DLC47` folder into the `\Dying Light\` directory.

![](/img/en/extract-the-package-including-dldxvk.png)

#

### 🌬️ Final notes 🗒️

+ In case you are unfamiliar with `DXVK`; There will be stutters for every un-rendered scene, as the shaders are compiled.
  - This stuttering should go away quickly. :)

+ There will be issues with `Fullscreen` using `DXVK`. Sadly this is not fixable for now.
	- There is a workaround, and that is to either:
	 - Swap to `Windowed` when necessary.
	 - Use `Windowed Borderless` instead.

+ Render distance will be more accurate. Slightly increase render distance to have old ranges.

+ This work is very tiring. So I hope you enjoy it. :3
	- ![](img/sleepy-cat.gif)


#
</details><!--English::Installation::Development::Windows::Steam-->

#
</details><!--English::Installation::Development::Windows-->

#
</details><!--English::Installation::Development-->

#
</details><!--English::Installation-->

#
</details><!--English-->

<details><!--Український-->
<summary><h2>🇺🇦 🇪🇺 :sunflower: Український :bullettrain_front: 🇪🇺 🇺🇦</h2></summary>

## 🍃 Dying Light Improved Compatibility Support Module (*`dimcsm`*)

[`dlim`](https://gitlab.com/dlimi/dlim)-базоване підвищення ефективності та продуктивності для [**Dying Light**](https://dl1.dyinglightgame.com).
#

<details><!--Український::Встановлення-->
<summary><h2>📥 Встановлення 🗜️</h2></summary>

<details><!--Український::Встановлення::Стабільний-->
<summary><h2>💼 Стабільний 📥</h2></summary>

#
</details><!--Український::Встановлення::Стабільний-->

<details><!--Український::Встановлення::Розробка-->
<summary><h2>🚧 Розробка 📥</h2></summary>

<details><!--Український::Встановлення::Розробка::Linux/Лінукс-->
<summary><h2>🚧 Linux/Лінукс 🐧</h2></summary>

<details><!--Український::Встановлення::Розробка::Linux::Steam з Proton-->
<summary><h3>⚛️ Steam з Proton 🐧</h3></summary>

#### `1`: У Steam, клацніть правою кнопкою миші `Dying Light` і перейдіть в розділ `Властивості`.

![](img/ua/перейдіть-до-властивості.png)

#### `2`: У розділі `Властивості`, перейдіть до розділу `СУМІСНІСТЬ`.

![](img/ua/перейдіть-до-сумісність.png)

#### `3`: Увімкнути Протон

![](/img/ua/увімкнути-протон.png)

*В ідеалі використовувати [`GE-Proton`](https://github.com/gloriouseggroll/proton-ge-custom).*

#### `4`: Перейдіть до розділу `ЗАГАЛЬНЕ`.

![](img/ua/перейдіть-до-загальне.png)

#### `5`: Налаштуйте параметри запуску, щоб увімкнути ключові функції:

```sh
PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0  %command% -nologos -fsreadmembuffer
```

> #### ⚠️ Зауважте! ❕
>
> Рекомендовано використовувати `DXVK_ASYNC`.
>
> Це значно знижує початкове заїкуватість для `pipeline-state-cache`.
>
> Однак для цього потрібно [`GE-Proton`](https://github.com/gloriouseggroll/proton-ge-custom), або [`DLDXVK`](https://gitlab.com/dlimi/dldxvk).
>
> *Не рекомендується використовувати `DLDXVK` безпосередньо, використовуйте тільки файл `dxvk.conf`, включений у збірку, з `GE-Proton`.*

<details><!--Український::Встановлення::Розробка::Linux/Лінукс::Steam з Proton::Твіки Nvidia-->
<summary><h4>🐸 Твіки Nvidia ⚡</h4></summary>

> #### Зауважте!
>
> Додайте `PROTON_HIDE_NVIDIA_GPU=0` та `PROTON_ENABLE_NVAPI=1` до параметрів запуску:
>
> ```sh
> PROTON_HIDE_NVIDIA_GPU=0 PROTON_ENABLE_NVAPI=1 PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0  %command% -nologos -fsreadmembuffer
> ```
#
</details><!--Український::Встановлення::Розробка::Linux/Лінукс::Steam з Proton::Твіки Nvidia-->

<details><!--Український::Встановлення::Розробка::Linux/Лінукс::Steam з Proton::FSRHack/FSRЗламати-->
<summary><h4>📹 FSRHack/FSRЗламати 🔍</h4></summary>

> #### Зауважте!
>
> Щоб отримати FSR, спочатку додайте `WINE_FULLSCREEN_FSR=1` та `WINE_FULLSCREEN_FSR=2` (*від `0`, найсильніший, до `5`, найслабший*) до опцій запуску:
>
> ```sh
> WINE_FULLSCREEN_FSR=1 WINE_FULLSCREEN_FSR_STRENGTH=2 PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0  %command% -nologos -fsreadmembuffer
> ```
>
> Або з GPU Nvidia:
>
> ```sh
> WINE_FULLSCREEN_FSR=1 WINE_FULLSCREEN_FSR_STRENGTH=2 PROTON_HIDE_NVIDIA_GPU=0 PROTON_ENABLE_NVAPI=1 PROTON_NO_D3D11=0 PROTON_NO_ESYNC=0 PROTON_NO_FSYNC=0  %command% -nologos -fsreadmembuffer
> ```
#
</details><!--Український::Встановлення::Розробка::Linux/Лінукс::Steam з Proton::FSRHack/FSRЗламати-->

#### `6`: Завантажте складання:

Файл                                                                                                                    | Опис
------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------
[`dlimcsm-balance.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-balance.pkg.7z?inline=false)         | Чисто збалансовані оптимізації з мінімальними компромісами.
[`dlimcsm-performance.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-performance.pkg.7z?inline=false) | Повна оптимізація продуктивності за максимального компромісу.

#### `7`: У папці Завантаження двічі клацніть файл `.7z`, щоб відкрити його.

![](img/ua/відкрити-архів.png)

#### `8`: Киньте файл `dxvk.conf` та `DW_DLC47` у каталог `/Dying Light/`.

![](img/ua/відкрити-архів.png)

#


### Підсумкові нотатки

+ Якщо ви не знайомі з `DXVK`, то при компіляції шейдерів у кожній нерендерованій сцені виникатимуть заїкання.
	- Заїкуватість має швидко пройти. :)

+ Відстань рендерингу буде точнішою. Небагато збільште відстань рендерингу, щоб мати старі діапазони. :3
	- ![](/img/sleepy-cat.gif)

#
</details><!--Український::Встановлення::Розробка::Linux/Лінукс::Steam з Proton-->

#
</details><!--Український::Встановлення::Розробка::Linux/Лінукс-->

<details><!--Український::Встановлення::Розробка::Windows-->
<summary><h2>🚧 Windows 🪟</h2></summary>

<details><!--Український::Встановлення::Розробка::Windows::Steam-->
<summary><h3>⚛️ Steam 🪟</h3></summary>

#### `1`: Встановіть `7zip`, якщо його не встановлено.

Якщо у вас немає `7zip`, [натисніть тут](https://7-zip.org/a/7z2107-x64.exe), щоб завантажити програму установки. Потім запустіть його з папки `/Downloads`.

#### `2`: Натисніть `WIN+R`, введіть `sysdm.cpl` та натисніть `OK`.

![](img/en/navigate-to-advanced-system-settings.png)

*Переклад відсутній, тому що на даний момент я не можу отримати доступ до Windows.*

#### `3`: Перейдіть до розділу `Змінні Oточення`.

![](img/en/navigate-to-env-vars.png)

*Переклад відсутній, тому що на даний момент я не можу отримати доступ до Windows.*

#### `4`: Знайдіть внизу `Системні Змінні` та створіть `Новий...`.

![](img/en/navigate-to-sys-vars.png)

*Переклад відсутній, тому що на даний момент я не можу отримати доступ до Windows.*

#### `5`: Встановіть ім'я `DXVK_ASYNC` та значення `1`.

![](img/en/create-the-var.png)

*Переклад відсутній, тому що на даний момент я не можу отримати доступ до Windows.*

##### Результат має виглядати так.

![](img/en/reference.png)

#### `6`: Завантажте складання:

Файл                                                                                                                                  | Опис
--------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------
[`dlimcsm-balance-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-balance-dldxvk.pkg.7z?inline=false)         | Чисто збалансовані оптимізації з мінімальними компромісами, включаючи `DLDXVK`.
[`dlimcsm-performance-dldxvk.pkg.7z`](https://gitlab.com/dlimi/dlimcsm/-/raw/main/pkg/dlimcsm-performance-dldxvk.pkg.7z?inline=false) | Повна оптимізація продуктивності з максимальним компромісом, включаючи `DLDXVK`.


#### `7`: У папці Завантаження двічі клацніть файл `.7z`, щоб відкрити його.

![](img/ua/відкрийте-архів-що-містить-dldxvk.png)

#### `8`: Закиньте файли `dxgi.dll`, `d3d11.dll`, `dxvk.conf` та папку `DW_DLC47` у каталог `\Dying Light\`.

![](img/ua/вийміть-пакет-який-включає-dldxvk.png)

#

### 🌬️ Підсумкові нотатки 🗒️

+ Якщо ви не знайомі з `DXVK`, то при компіляції шейдерів у кожній нерендерованій сцені виникатимуть заїкання.
  - Заїкуватість швидко пройде. (*Особливо при використанні `DXVK_ASNYC`.*)

 + Будуть проблеми з `Fullscreen` при використанні `DXVK`. На жаль, зараз це неможливо виправити.
	- Існує обхідний шлях, і він полягає в тому, щоб або:
	 - При необхідності перейдіть на `Вікно`.
	 - Натомість використовуйте `Віконний режим без рамки`.

+ Відстань рендерингу буде точнішою. Небагато збільште відстань рендерингу, щоб мати старі діапазони. =)
	- ![](/img/sleepy-cat.gif)

#
</details><!--Український::Встановлення::Розробка::Windows::Steam-->

#
</details>

#
</details><!--Український::Встановлення::Розробка-->

#
</details><!--Український::Встановлення-->

#
</details><!--Український-->
